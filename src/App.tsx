import React from 'react';
import {CssBaseline} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import TopAppBar from './TopAppBar';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import ProgramDashboard from './ProgramDashboard';
import ProgramSelection from './ProgramSelection';

const useStyles = makeStyles(theme => ({
  main: {
    marginTop: theme.spacing(6),
    marginBottom: theme.spacing(6),
    padding: theme.spacing(2),
  }
}));

function App() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <CssBaseline />
      <div>
        <TopAppBar/>
        <main className={classes.main}>
          <Router>
            <Route path="/dashboard" component={ProgramDashboard} />
            <Route path="/programs" component={ProgramSelection} />
          </Router>
        </main>
      </div>
    </React.Fragment>
  );
}

export default App;
