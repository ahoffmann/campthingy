import React from 'react'
import Container from '@material-ui/core/Container';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import {makeStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
    padding: theme.spacing(2),
  },
  header: {
    marginBottom: theme.spacing(6),
  },
  actionButton: {
    float: 'right',
    [theme.breakpoints.down('sm')]: {
      float: 'left',
    }
  },
  headerText: {
    float: 'left',
  }
}));

function ProgramSelection() {
  const classes = useStyles();
  return (
    <Container maxWidth="md" className={classes.root}>
      <div className={classes.header}>
        <Typography variant="h5" className={classes.headerText}>
          Program Selection
        </Typography>
        <Button variant="contained" color="primary" className={classes.actionButton}>
          Register New Program
        </Button>
      </div>
      <List>
        <ListItem alignItems="flex-start" button>
          <ListItemText primary='Badger Sports Camps - Basetball' secondary='07/15/2019 - 07/19/2019'/>
        </ListItem>
        <Divider/>
        <ListItem alignItems="flex-start" button>
          <ListItemText primary='Badger Sports Camps - Football' secondary='07/08/2019 - 07/12/2019'/>
        </ListItem>
      </List>
    </Container>
  );
}

export default ProgramSelection;
