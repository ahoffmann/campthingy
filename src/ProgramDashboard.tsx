import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import WarningRoundedIcon from '@material-ui/icons/WarningRounded';
import ErrorRoundedIcon from '@material-ui/icons/ErrorRounded';
import CheckCircleRoundedIcon from '@material-ui/icons/CheckCircleRounded';
import PersonRoundedIcon from '@material-ui/icons/PersonRounded';
import LinearProgress from '@material-ui/core/LinearProgress';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
    padding: theme.spacing(2),
  },
  rootGrid: {
    flexGrow: 1,
  },
  errorIcon: {
    float: 'right',
    color: 'red',
  },
  warningIcon: {
    float: 'right',
    color: 'orange',
  },
  successIcon: {
    float: 'right',
    color: 'green',
  },
  progressText: {
    float: 'right',
  },
  personIcon: {
    float: 'right',
  }
}));

function ProgramDashboard() {
  const classes = useStyles();

  return (
    <Container maxWidth="lg" className={classes.root}>
      <Typography gutterBottom variant="h4" component="h2">
        Example Program
      </Typography>
      <Grid container className={classes.rootGrid} spacing={2} justify="space-evenly">
        <Grid item justify="center" xs={12} md={6} lg={3}>
          <Card>
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                RES Letter
                <ErrorRoundedIcon className={classes.errorIcon}/>
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                RES Letter Needs Submission!
              </Typography>
            </CardContent>
            <CardActions>
              <Button size="small" color="primary">Upload</Button>
              <Button size="small" color="primary">Learn More</Button>
            </CardActions>
          </Card>
        </Grid>
        <Grid item justify="center" xs={12} md={6} lg={3}>
          <Card>
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                Saftey Plan
                <WarningRoundedIcon className={classes.warningIcon}/>
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                You have not attested to having a saftey plan.
              </Typography>
            </CardContent>
            <CardActions>
              <Button size="small" color="primary">Attest</Button>
              <Button size="small" color="primary">Learn More</Button>
            </CardActions>
          </Card>
        </Grid>
        <Grid item justify="center" xs={12} md={12} lg={6}>
          <Card>
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                Complience Percentage
                <Typography gutterBottom variant="h5" component="h2" className={classes.progressText}>
                  70%
                </Typography>
              </Typography>
              <LinearProgress
                variant="determinate"
                color="primary"
                value={70}
              />
            </CardContent>
            <CardActions>
              <Button size="small" color="primary">What is left?</Button>
            </CardActions>
          </Card>
        </Grid>
        <Grid item justify="center" xs={12} md={6} lg={4}>
          <Card>
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                Training
                <CheckCircleRoundedIcon className={classes.successIcon}/>
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                All of your staff training is up to date!
              </Typography>
            </CardContent>
            <CardActions>
              <Button size="small" color="primary">View Staff</Button>
              <Button size="small" color="primary">Learn More</Button>
            </CardActions>
          </Card>
        </Grid>
        <Grid item justify="center" xs={12} md={6} lg={4}>
          <Card>
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                Participant/Staff Ratio
                <CheckCircleRoundedIcon className={classes.successIcon}/>
              </Typography>
              <Typography variant="h6" color="textSecondary" component="p">
                Ratio: 5.6
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                The staff to participant ratio must be under 6 to be in complience.
              </Typography>
            </CardContent>
            <CardActions>
              <Button size="small" color="primary">Learn More</Button>
            </CardActions>
          </Card>
        </Grid>
        <Grid item justify="center" xs={12} md={6} lg={4}>
          <Card>
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                84 Participants
                <PersonRoundedIcon className={classes.personIcon}/>
              </Typography>
            </CardContent>
            <CardActions>
              <Button size="small" color="primary">View/Edit Roster</Button>
            </CardActions>
          </Card>
        </Grid>
      </Grid>
    </Container>
  );
}

export default ProgramDashboard;
